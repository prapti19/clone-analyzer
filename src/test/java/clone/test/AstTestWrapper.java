package clone.test;

import ast.Expr;
import ast.ExprStmt;
import ast.Program;
import ast.Sprite;
import ast.Stage;
import sb3.dsl.SimXml2AstParser;

public class AstTestWrapper {
	private final Program program;

	public AstTestWrapper(Program program) {
		this.program = program;
	}
	
	public Program getProgram() {
		return program;
	}
	
	public static Expr exprOf(String exprStr) {
		return (Expr) new SimXml2AstParser().parse(exprStr);
	}
	
	public static ExprStmt exprStmtOf(String str) {
		return (ExprStmt) new SimXml2AstParser().parse(str);
	}

	public static AstTestWrapper testProgramOf(String... scripts) {
		return new AstTestWrapper(programOf(scripts));
	}
	
	public static Program programOf(String... scripts) {
		String simpleProgramStr = "<program><sprite>%s</sprite></program>";
		StringBuilder sb = new StringBuilder();
		for (String script : scripts) {
			sb.append(script);
		}
		Program program = (Program) new SimXml2AstParser().parse(String.format(simpleProgramStr, sb.toString()));
		return program;
	}

	public Sprite firstSprite() {
		return program.getStage().getSprite(0);
	}

	public static AstTestWrapper testProgramOf(Sprite sprite) {
		return new AstTestWrapper(programOf(sprite));
	}
	
	public static Program programOf(Sprite sprite) {
		Program program = new Program();
		program.setStage(new Stage());
		program.getStage().addSprite(sprite);
		return program;
	}

	
}
