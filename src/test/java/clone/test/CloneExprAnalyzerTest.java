package clone.test;

import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

import java.net.MalformedURLException;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.core.config.Configurator;
import org.junit.BeforeClass;
import org.junit.Test;

import ast.Expr;
import ast.OperatorExpr;
import ast.Program;
import ast.Sprite;
import clone.analyzer.CloneExprAnalyzer;
import clone.data.CExpr;
import clone.result.CloneExprResult;
import sb3.dsl.SimXml2AstParser;
import sb3.parser.xml.Sb3XmlParser;

public class CloneExprAnalyzerTest {
	private static Logger LOGGER = null;

	@BeforeClass
	public static void setLogger() throws MalformedURLException {
		LOGGER = LogManager.getLogger();
		Configurator.setRootLevel(Level.OFF);
	}

	@Test
	public void testLookupNodeById1() {
		SimXml2AstParser parser = new SimXml2AstParser();
		Sprite sprite = (Sprite) parser
				.parse("<sprite><script><stmt><expr type='op1'><expr id='e1'></expr></expr></stmt></script></sprite>");
		Program program = AstTestWrapper.programOf(sprite);
		Optional<OperatorExpr> res = program.lookupOpExpr("e1");
		assertThat(res.isPresent(), equalTo(true));
	}

	@Test
	public void testLookupNodeById2() {
		String xml = "<xml xmlns=\"http://www.w3.org/1999/xhtml\"><variables></variables><block type=\"motion_movesteps\" id=\"UgnwT`Z@JQw{^2,Cp;,1\" x=\"91\" y=\"229\"><value name=\"STEPS\"><shadow type=\"math_number\" id=\"s!w)]AOedHdf`b4Ow_`h\"><field name=\"NUM\">10</field></shadow><block type=\"operator_random\" id=\":DET^_^}^2=Gx`!O?Oav\"><value name=\"FROM\"><shadow type=\"math_number\" id=\"8[:+(oyL_ENSF]=`T::k\"><field name=\"NUM\">1</field></shadow></value><value name=\"TO\"><shadow type=\"math_number\" id=\"x6`};e]aQpoSVksZ)h,8\"><field name=\"NUM\">10</field></shadow><block type=\"operator_add\" id=\"R0qLjWWMPM/fjzh8V0Ey\"><value name=\"NUM1\"><shadow type=\"math_number\" id=\"~@gTHYxXNbTie0*p6V$|\"><field name=\"NUM\">2</field></shadow></value><value name=\"NUM2\"><shadow type=\"math_number\" id=\"ywzcda=Thgqpe06k8R^j\"><field name=\"NUM\">3</field></shadow></value></block></value></block></value></block><block type=\"motion_pointindirection\" id=\"M8`Kg9?n}Sak=yAmg=DC\" x=\"567\" y=\"228\"><value name=\"DIRECTION\"><shadow type=\"math_angle\" id=\"%`8dP~]zytWP_?NoZ|nx\"><field name=\"NUM\">90</field></shadow><block type=\"operator_random\" id=\"QJ^GU))@8jM8D%w[BdiT\"><value name=\"FROM\"><shadow type=\"math_number\" id=\"WsPZy-H/5Xa:7h00C-h/\"><field name=\"NUM\">1</field></shadow></value><value name=\"TO\"><shadow type=\"math_number\" id=\"4-4pg8Wu;PwyJmuGRdoi\"><field name=\"NUM\">10</field></shadow><block type=\"operator_add\" id=\"*~5$psbD8[|5yQX#,bQ{\"><value name=\"NUM1\"><shadow type=\"math_number\" id=\"R06#2rC?q|Q0S:/Ic76O\"><field name=\"NUM\">2</field></shadow></value><value name=\"NUM2\"><shadow type=\"math_number\" id=\"#}SU!@mb1^e|;52HlfA#\"><field name=\"NUM\">3</field></shadow></value></block></value></block></value></block></xml>";
		Sb3XmlParser sb3Parser = new Sb3XmlParser();
		Sprite sprite = sb3Parser.parseTarget(xml);
		Program program = AstTestWrapper.programOf(sprite);
		Optional<OperatorExpr> res = program.lookupOpExpr(":DET^_^}^2=Gx`!O?Oav");
		assertThat(res.isPresent(), equalTo(true));
	}

	@Test
	public void testRootExprTree() {
		SimXml2AstParser parser = new SimXml2AstParser();
		Sprite sprite = (Sprite) parser.parse(
				"<sprite><script><stmt><expr id='1' type='a'><expr id='1.1' type='b'></expr><expr id='1.2' type='c'><expr id='1.2.1' type='d'></expr></expr></expr></stmt></script></sprite>");
		Program program = AstTestWrapper.programOf(sprite);
		OperatorExpr rootExpr = program.lookupOpExpr("1").get();
		OperatorExpr child1_1 = program.lookupOpExpr("1.1").get();
		OperatorExpr child1_2_1 = program.lookupOpExpr("1.2.1").get();
		assertThat(rootExpr.isRootExpr(), equalTo(true));
		assertThat(child1_1.rootExpr(), equalTo(rootExpr));
		assertThat(child1_2_1.rootExpr(), equalTo(rootExpr));
	}

	@Test
	public void testSize() {
		SimXml2AstParser parser = new SimXml2AstParser();
		Expr expr1 = (Expr) parser.parse("<expr><num>1</num><expr><num>4</num></expr></expr>");
		Expr expr2 = (Expr) parser.parse("<expr><expr><num>4</num></expr></expr>");
		assertTrue(new CExpr(expr1).size() > new CExpr(expr2).size());
	}

	@Test
	public void testCloneExpr() {
		SimXml2AstParser parser = new SimXml2AstParser();
		Sprite sprite = (Sprite) parser.parse(
				"<sprite><script><stmt><expr id='1' type='a'><expr id='2' type='b'><num>1</num></expr></expr></stmt><stmt><expr id='3' type='a'><expr id='4' type='b'><num>1</num></expr></expr></stmt></script></sprite>");
		CloneExprAnalyzer analyzer = new CloneExprAnalyzer();
		Map<Expr, List<CExpr>> res = analyzer.analyze(AstTestWrapper.programOf(sprite));
		LOGGER.debug(res);

		CloneExprResult result = new CloneExprResult(res);
		assertThat(result.numGroup(), equalTo(1));
		assertThat(result.firstGroup().size(), equalTo(2));
		assertThat(result.firstGroup().firstInstance().asNameSeq(), contains("a", "b", "1"));
	}

	@Test
	public void testCloneExpr2() {
		Sb3XmlParser sb3Parser = new Sb3XmlParser();
		Sprite sprite = sb3Parser.parseTarget(
				"<xml xmlns=\"http://www.w3.org/1999/xhtml\"><variables></variables><block type=\"motion_movesteps\" id=\"(Q5ph7?vGm+:/6wC]44E\" x=\"148\" y=\"147\"><value name=\"STEPS\"><shadow type=\"math_number\" id=\"tJu~}j$qp}d9;%q=5B5{\"><field name=\"NUM\">10</field></shadow><block type=\"operator_random\" id=\"Rt8Fds77WpFZ44Z;0=q[\"><value name=\"FROM\"><shadow type=\"math_number\" id=\"gB2~A2ss^%!Lm|4H@kZ9\"><field name=\"NUM\">1</field></shadow></value><value name=\"TO\"><shadow type=\"math_number\" id=\"ZkILx,~,|xh?5RJJc:}*\"><field name=\"NUM\">10</field></shadow><block type=\"operator_length\" id=\"3v)AY`/QYw=YumEO-2ph\"><value name=\"STRING\"><shadow type=\"text\" id=\"y*r2Mb!Xx3ux5o{c4:Qa\"><field name=\"TEXT\">world</field></shadow></value></block></value></block></value><next><block type=\"motion_turnright\" id=\"JK|77ipA{}kp%5bJVb;d\"><value name=\"DEGREES\"><shadow type=\"math_number\" id=\"as%)4njC~$l;7,du^`D{\"><field name=\"NUM\">15</field></shadow><block type=\"operator_letter_of\" id=\"zbyM3^7S!!?1+?[jV`kX\"><value name=\"LETTER\"><shadow type=\"math_whole_number\" id=\"=u6ezH}BzGpVZ2DWM?3B\"><field name=\"NUM\">1</field></shadow><block type=\"operator_random\" id=\"bUJ5K1q`VqvCJ;6djImb\"><value name=\"FROM\"><shadow type=\"math_number\" id=\"(L`b8@M_$w-~|Oc8{_KG\"><field name=\"NUM\">1</field></shadow></value><value name=\"TO\"><shadow type=\"math_number\" id=\"qA7:-HV0D{aRS+%2m[IB\"><field name=\"NUM\">10</field></shadow><block type=\"operator_length\" id=\",TClb}*}0E;Ao=4txi`G\"><value name=\"STRING\"><shadow type=\"text\" id=\"_m!2/g+]WFu$6vLRC`W!\"><field name=\"TEXT\">world</field></shadow></value></block></value></block></value><value name=\"STRING\"><shadow type=\"text\" id=\"w1(W!@K2;fImc]7hE}wy\"><field name=\"TEXT\">world</field></shadow></value></block></value></block></next></block></xml>");
		Program program = AstTestWrapper.programOf(sprite);
		System.out.println(program.print());
		CloneExprAnalyzer analyzer = new CloneExprAnalyzer();
		Map<Expr, List<CExpr>> res = analyzer.analyze(program);
		CloneExprResult result = new CloneExprResult(res);

		assertThat(result.numGroup(), equalTo(1));
		assertThat(result.firstGroup().size(), equalTo(2));
		LOGGER.debug(result.firstGroup().firstInstance().asNameSeq());
		assertThat(result.firstGroup().firstInstance().asNameSeq(),
				contains("operator_random", "1", "operator_length", "world"));
	}

	@Test
	public void testCloneExpr3() {
		Sb3XmlParser sb3Parser = new Sb3XmlParser();
		String xml = "<program><stage name=\"Stage\"><xml><variables><variable type=\"\" id=\"B)Sa^NF8*FRfBi=]*VG}-a\">a</variable></variables><block id=\"~D=_aD+3G}c^PQ(t*.#Q\" type=\"looks_nextbackdrop\" x=\"117\" y=\"327.8\" ></block></xml></stage><sprite name=\"Sprite1\"><xml><variables><variable type=\"\" id=\"C+~@[SO+Pq];7nvT[xsW-b\">b</variable></variables><block id=\"y@JhECx|pJ{Hs5wi(O^1\" type=\"motion_movesteps\" x=\"286.5\" y=\"349.8\" ><value name=\"STEPS\"><block id=\"O#ydaL2ZG*YXAn,arG=_\" type=\"operator_random\" ><value name=\"FROM\"><shadow id=\"gQ;OM:w2KK7}`aKrMBv5\" type=\"math_number\" ><field name=\"NUM\">1</field></shadow></value><value name=\"TO\"><block id=\".HG1;dcIK~=~c_w+v+ax\" type=\"operator_add\" ><value name=\"NUM1\"><block id=\"W!{^U#b1Wb5GqG#Ekia`\" type=\"operator_multiply\" ><value name=\"NUM1\"><shadow id=\"+IW2hoYVy3?DTxz%?]mB\" type=\"math_number\" ><field name=\"NUM\">1</field></shadow></value><value name=\"NUM2\"><shadow id=\"81)6Q:uS~aF#/+er-R`@\" type=\"math_number\" ><field name=\"NUM\">2</field></shadow></value></block><shadow id=\"Fnvo4%puG?@#iz-kHNhR\" type=\"math_number\" ><field name=\"NUM\">10</field></shadow></value><value name=\"NUM2\"><block id=\"tvAcMOW_/B./wFu6HB5.\" type=\"operator_mod\" ><value name=\"NUM1\"><shadow id=\"+3Y/s5;E_p?iG23/Vh[b\" type=\"math_number\" ><field name=\"NUM\">24</field></shadow></value><value name=\"NUM2\"><shadow id=\"Qob*5zf2)QgDoK7m_vYg\" type=\"math_number\" ><field name=\"NUM\">5</field></shadow></value></block><shadow id=\"EDVX]k-2l#2m[o5*FxQz\" type=\"math_number\" ><field name=\"NUM\">10</field></shadow></value></block><shadow id=\"O^^IVL_||,ALO}/bmLT?\" type=\"math_number\" ><field name=\"NUM\">10</field></shadow></value></block><shadow id=\"YyP1s#43~:4;CByl[iYm\" type=\"math_number\" ><field name=\"NUM\">10</field></shadow></value></block></xml></sprite><sprite name=\"Sprite2\"><xml><variables></variables><block id=\"chbxMdNI[ac!L=p!Bt9)\" type=\"motion_turnright\" x=\"313.5\" y=\"349.8\" ><value name=\"DEGREES\"><block id=\"X`ZqKapKVc=uTQsR+PUO\" type=\"operator_random\" ><value name=\"FROM\"><shadow id=\"w9/.A(YNO-/CrATm{mQ0\" type=\"math_number\" ><field name=\"NUM\">2</field></shadow></value><value name=\"TO\"><block id=\"p`GIKU{/F7eq1.w6V5DN\" type=\"operator_add\" ><value name=\"NUM1\"><block id=\"M{thUPjRz[xktb/,b)q#\" type=\"operator_multiply\" ><value name=\"NUM1\"><shadow id=\"IP#A%Wf//yR}y61OI]CI\" type=\"math_number\" ><field name=\"NUM\">1</field></shadow></value><value name=\"NUM2\"><shadow id=\"I~YwboCM/m}TMVW`;x2_\" type=\"math_number\" ><field name=\"NUM\">2</field></shadow></value></block><shadow id=\"XEes=O1Xg+vYEP1{#K;e\" type=\"math_number\" ><field name=\"NUM\">10</field></shadow></value><value name=\"NUM2\"><block id=\"C*]IwhD4.,EC4f:gdV;C\" type=\"operator_mod\" ><value name=\"NUM1\"><shadow id=\"cb)eb`}wqroE)5pOKRqg\" type=\"math_number\" ><field name=\"NUM\">24</field></shadow></value><value name=\"NUM2\"><shadow id=\"|bvsVSiO5|+d}8:(K@w7\" type=\"math_number\" ><field name=\"NUM\">5</field></shadow></value></block><shadow id=\".q:,7U/zht0jOZ4Iu_nl\" type=\"math_number\" ><field name=\"NUM\">10</field></shadow></value></block><shadow id=\"8T/]/=gKo4*`A_YtX.ig\" type=\"math_number\" ><field name=\"NUM\">10</field></shadow></value></block><shadow id=\"M`cK(kG81%FgubH!7bd6\" type=\"math_number\" ><field name=\"NUM\">10</field></shadow></value></block></xml></sprite></program>";
		Program program = sb3Parser.parseProgram(xml);
		CloneExprAnalyzer analyzer = new CloneExprAnalyzer();
		Map<Expr, List<CExpr>> res = analyzer.analyze(program);
		CloneExprResult result = new CloneExprResult(res);
		assertThat(result.numGroup(), equalTo(1));
		assertThat(result.firstGroup().size(), equalTo(2));
		LOGGER.debug(result.firstGroup().firstInstance().asNameSeq());
		assertThat(result.firstGroup().firstInstance().asNameSeq(),
				contains("operator_add", "operator_multiply", "1", "2", "operator_mod", "24", "5"));
		OperatorExpr rootExpr = (OperatorExpr) result.firstGroup().firstInstance().get();
		LOGGER.debug(rootExpr.getBlockId());
	}

	/*
	 * TODO: ignore small expression
	 */

}
