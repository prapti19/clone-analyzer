package clone.test;

import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.not;
import static org.junit.Assert.assertThat;

import java.net.MalformedURLException;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.core.config.Configurator;
import org.junit.BeforeClass;
import org.junit.Test;

import ast.BlockSeq;
import ast.ExprStmt;
import ast.Program;
import ast.Sprite;
import ast.Stmt;
import clone.analyzer.CloneStmtSeqAnalyzer;
import clone.analyzer.ASTCollectorFunctions;
import clone.data.CStmtSeq;
import clone.result.CloneSeqResult;
import sb3.dsl.SimXml2AstParser;
import sb3.parser.json.Sb3Json2Block;
import sb3.parser.xml.Sb3XmlParser;

public class CloneStmtSeqAnalyzerTest {
	private static Logger LOGGER = null;

	@BeforeClass
	public static void setLogger() throws MalformedURLException {
		LOGGER = LogManager.getLogger();
		Configurator.setRootLevel(Level.OFF);
	}

	@Test
	public void testSubSeq() {
		SimXml2AstParser parser = new SimXml2AstParser();
		Sprite sprite = (Sprite) parser.parse(
				"<sprite><script><stmt type='1'></stmt><stmt type='2'></stmt><stmt type='1'></stmt><stmt type='2'></stmt></script></sprite>");
		Program program = AstTestWrapper.programOf(sprite);
		ASTCollectorFunctions collector = new ASTCollectorFunctions();
		HashSet<BlockSeq> seqColl = program.blockSeqColl();
		List<CStmtSeq> stmtSeqList = collector.collect(seqColl.iterator().next());
		assertThat(stmtSeqList.size(), equalTo(6));
	}

	@Test
	public void testASTNodeHash() {
		SimXml2AstParser parser = new SimXml2AstParser();
		Stmt stmt = (Stmt) parser.parse("<stmt type='1'></stmt>");
		Stmt stmt2 = (Stmt) parser.parse("<stmt type='1'></stmt>");
		assertThat(stmt.hashCode(), not(equalTo(stmt2.hashCode())));

	}

	private List<CStmtSeq> testInput() {
		Program program = getTestSimProgram(
				"<sprite><script><stmt type='1'></stmt><stmt type='2'></stmt><stmt type='3'></stmt><stmt type='1'></stmt><stmt type='2'></stmt><stmt type='3'></stmt></script></sprite>");
		ASTCollectorFunctions collector = new ASTCollectorFunctions();
		HashSet<BlockSeq> seqColl = program.blockSeqColl();
		List<CStmtSeq> stmtSeqList = collector.collect(seqColl.iterator().next());
		return stmtSeqList;
	}

	private Program getTestSimProgram(String xmlStr) {
		SimXml2AstParser parser = new SimXml2AstParser();
		Sprite sprite = (Sprite) parser.parse(xmlStr);
		Program program = AstTestWrapper.programOf(sprite);
		return program;
	}

	@Test
	public void testSimpleSequence() {
		/*
		 * stmt1,stmt2,stmt3, stmt1,stmt2,stmt3
		 */
		CloneStmtSeqAnalyzer analyzer = new CloneStmtSeqAnalyzer();
		Program program = getTestSimProgram(
				"<sprite><script><stmt type='1'></stmt><stmt type='2'></stmt><stmt type='3'></stmt><stmt type='1'></stmt><stmt type='2'></stmt><stmt type='3'></stmt></script></sprite>");
		Map<BlockSeq, List<CStmtSeq>> res = analyzer.analyze(program);
		assertThat(res.size(), equalTo(1));
		assertThat(res.entrySet().iterator().next().getValue().size(), equalTo(2));
		assertThat(res.entrySet().iterator().next().getValue().iterator().next().size(), equalTo(3));
	}

	@Test
	public void testSameSeqDifferentInput() {
		/*
		 * stmt1(1),stmt2,stmt3, stmt1(2),stmt2,stmt3
		 */
		CloneStmtSeqAnalyzer analyzer = new CloneStmtSeqAnalyzer();
		Program program = getTestSimProgram(
				"<sprite><script><stmt type='1'><expr><num>1</num></expr></stmt><stmt type='2'></stmt><stmt type='3'></stmt><stmt type='1'><num>2</num></stmt><stmt type='2'></stmt><stmt type='3'></stmt></script></sprite>");
		Map<BlockSeq, List<CStmtSeq>> res = analyzer.analyze(program);
		LOGGER.debug(res);
		CloneSeqResult result = CloneSeqResult.of(res);
		assertThat(result.numGroup(), equalTo(1));
		assertThat(result.firstGroup().size(), equalTo(2));
		assertThat(result.firstGroup().firstInstance().asNameSeq(), contains("1", "2", "3"));
	}

	@Test
	public void testSameSeqDifferentInput2() {
		CloneStmtSeqAnalyzer analyzer = new CloneStmtSeqAnalyzer();
		Sb3XmlParser sb3Parser = new Sb3XmlParser();
		Sprite sprite = sb3Parser.parseTarget(
				"<xml xmlns=\"http://www.w3.org/1999/xhtml\"><variables></variables><block type=\"motion_glideto\" id=\"U0c0B%Dq7qOn5iX]@=cD\" x=\"127\" y=\"145\"><value name=\"SECS\"><shadow type=\"math_number\" id=\"{Z`i!S?p^/%lq.`2CA2~\"><field name=\"NUM\">1</field></shadow></value><value name=\"TO\"><shadow type=\"motion_glideto_menu\" id=\"R.;(;YE,QHd4o+-[l8o]\"><field name=\"TO\">_mouse_</field></shadow></value><next><block type=\"motion_movesteps\" id=\"N6[d}~,R/,80v:9cTUe8\"><value name=\"STEPS\"><shadow type=\"math_number\" id=\"R0TP{l$qn3#,k(k%{VHQ\"><field name=\"NUM\">10</field></shadow><block type=\"operator_random\" id=\"h.mo4tU#+ohk?TtXMpJV\"><value name=\"FROM\"><shadow type=\"math_number\" id=\"Pd5ZT5/aaTVsY+#c~-s?\"><field name=\"NUM\">1</field></shadow></value><value name=\"TO\"><shadow type=\"math_number\" id=\"kpdS37m(z2n^()}y/JYs\"><field name=\"NUM\">21</field></shadow></value></block></value><next><block type=\"motion_turnright\" id=\"un#.c{!mvw?f{;(zk8DC\"><value name=\"DEGREES\"><shadow type=\"math_number\" id=\"},vg$+#MvqWqnWTGfrG%\"><field name=\"NUM\">15</field></shadow></value><next><block type=\"motion_turnleft\" id=\"buZDYM7~U7h#T2#RM3l3\"><value name=\"DEGREES\"><shadow type=\"math_number\" id=\"s@B(d6Qjh:gR^{J-*ewq\"><field name=\"NUM\">24</field></shadow></value><next><block type=\"motion_changexby\" id=\"[5wt85,%lw~xW)Z=^?vE\"><value name=\"DX\"><shadow type=\"math_number\" id=\"^,A,y}ea/oxxR0z/Fn,$\"><field name=\"NUM\">10</field></shadow></value></block></next></block></next></block></next></block></next></block><block type=\"motion_gotoxy\" id=\"q]D+x{51*g|!f3P9FMF0\" x=\"560\" y=\"163\"><value name=\"X\"><shadow type=\"math_number\" id=\"i:|+A4#E7(Buf^9.Sv3.\"><field name=\"NUM\">0</field></shadow></value><value name=\"Y\"><shadow type=\"math_number\" id=\"ABJW$OeYF(!}BY^~K07}\"><field name=\"NUM\">0</field></shadow></value><next><block type=\"motion_movesteps\" id=\"UQuQl:$9Xl/,@OL%(GXy\"><value name=\"STEPS\"><shadow type=\"math_number\" id=\"A51`b0Dvc/5dja:vKG(A\"><field name=\"NUM\">10</field></shadow><block type=\"operator_random\" id=\"hX5n{^(lp#8b#I!mH[$-\"><value name=\"FROM\"><shadow type=\"math_number\" id=\"WC!r$8y8%zgmO8fMi]O:\"><field name=\"NUM\">1</field></shadow></value><value name=\"TO\"><shadow type=\"math_number\" id=\"5:Z?hKT2.85E5(`Q,B9S\"><field name=\"NUM\">10</field></shadow></value></block></value><next><block type=\"motion_turnright\" id=\"6@LPbkpf[tgaWFd@mR.p\"><value name=\"DEGREES\"><shadow type=\"math_number\" id=\"_r=FOTsJ,_.q0#6},,sf\"><field name=\"NUM\">15</field></shadow></value><next><block type=\"motion_turnleft\" id=\"Ir=B6!0aGh2RM|ISKoHJ\"><value name=\"DEGREES\"><shadow type=\"math_number\" id=\"8%`E:WVeZ6wy7],=DE)j\"><field name=\"NUM\">15</field></shadow></value><next><block type=\"motion_changeyby\" id=\"[13[vhze!-J3U`vlKoe3\"><value name=\"DY\"><shadow type=\"math_number\" id=\"PjRHO3R`r2`:krGQT,(E\"><field name=\"NUM\">10</field></shadow></value></block></next></block></next></block></next></block></next></block></xml>");
		Program program = AstTestWrapper.programOf(sprite);
		Map<BlockSeq, List<CStmtSeq>> res = analyzer.analyze(program);
		CloneSeqResult result = CloneSeqResult.of(res);
		assertThat(result.numGroup(), equalTo(1));
		assertThat(result.firstGroup().size(), equalTo(2));
		assertThat(result.firstGroup().firstInstance().asNameSeq(),
				contains("motion_movesteps", "motion_turnright", "motion_turnleft"));
	}

	@Test
	public void testNestedSeq() {
		CloneStmtSeqAnalyzer analyzer = new CloneStmtSeqAnalyzer();
		String spriteXml = "<sprite><script><ifelse><cond><str>true</str></cond><then><stmt type='c1'><num>1</num></stmt><stmt type='c2'></stmt><stmt type='c3'></stmt></then><else><stmt type='c1'><num>2</num></stmt><stmt type='c2'></stmt><stmt type='c3'></stmt></else></ifelse><stmt type='c1'><num>3</num></stmt><stmt type='c2'></stmt><stmt type='c3'></stmt></script></sprite>";
		Program program = getTestSimProgram(spriteXml);
		Map<BlockSeq, List<CStmtSeq>> res = analyzer.analyze(program);
		CloneSeqResult result = CloneSeqResult.of(res);
		assertThat(result.numGroup(), equalTo(1));
		assertThat(result.firstGroup().size(), equalTo(3));
		assertThat(result.firstGroup().firstInstance().asNameSeq(), contains("c1", "c2", "c3"));
		LOGGER.debug(result.firstGroup());
	}

	@Test
	public void testCloneSeq() {
		Sb3XmlParser sb3Parser = new Sb3XmlParser();
		String xml = "<program><stage name=\"Stage\"><xml><variables><variable type=\"\" id=\"R,CyMpJ|22A)l/!J[Fnx-a\">a</variable></variables><block id=\"=)sz/d%_hkYpMzqynDMR\" type=\"looks_nextbackdrop\" x=\"117\" y=\"327.8\" ></block></xml></stage><sprite name=\"Sprite1\"><xml><variables><variable type=\"\" id=\"rQI6]0Fenud#bO.qoS;W-b\">b</variable></variables><block id=\"QH.an1%`BFPp_IE%*},s\" type=\"control_repeat\" x=\"262.5\" y=\"228.8\" ><value name=\"TIMES\"><shadow id=\"RtUOiK8l1pgAX4k.F2%4\" type=\"math_whole_number\" ><field name=\"NUM\">10</field></shadow></value><value name=\"SUBSTACK\"><block id=\"Ki%nw`xv[+o|tB7eUn!2\" type=\"motion_movesteps\" ><value name=\"STEPS\"><shadow id=\"XmyMO.JYz_q)~_1Ntjm+\" type=\"math_number\" ><field name=\"NUM\">10</field></shadow></value><next><block id=\"|@z-143Iao%5;x]3Y0s:\" type=\"motion_turnright\" ><value name=\"DEGREES\"><shadow id=\"m.QLTg|SRBQg4ez;z?XM\" type=\"math_number\" ><field name=\"NUM\">20</field></shadow></value><next><block id=\"2rO5,-sUG%/As5WSr7PR\" type=\"motion_turnleft\" ><value name=\"DEGREES\"><block id=\"Npj~=3p@5-i-dx;Z^*V/\" type=\"operator_mathop\" ><value name=\"NUM\"><shadow id=\"7bAF47t4y#QAFK#2uUVz\" type=\"math_number\" ><field name=\"NUM\">9</field></shadow></value><field name=\"OPERATOR\">sqrt</field></block><shadow id=\"I,:ZD23`~GeW/7:Mc.e0\" type=\"math_number\" ><field name=\"NUM\">10</field></shadow></value></block></next></block></next></block></value></block>,<block id=\"p*4^CbSpHq-9k.nX]No=\" type=\"control_forever\" x=\"697.5\" y=\"226.60000000000002\" ><value name=\"SUBSTACK\"><block id=\"*AMfGE@f@t2J}%-]mGFy\" type=\"motion_movesteps\" ><value name=\"STEPS\"><block id=\"_De*lJ(vrNe+`5m}Q,XH\" type=\"operator_random\" ><value name=\"FROM\"><shadow id=\"e92ZUV[-lZM~R!Py(|RT\" type=\"math_number\" ><field name=\"NUM\">1</field></shadow></value><value name=\"TO\"><shadow id=\"c0~Z7GC;gXs|=q)UW{~Q\" type=\"math_number\" ><field name=\"NUM\">10</field></shadow></value></block><shadow id=\"x7=]7R(7{Us3W44d9{l/\" type=\"math_number\" ><field name=\"NUM\">10</field></shadow></value><next><block id=\"9y[B?+|hQ{_]Bjx!Gyw^\" type=\"motion_turnright\" ><value name=\"DEGREES\"><shadow id=\"w6~lcz_2_m8a,hF%~-)*\" type=\"math_number\" ><field name=\"NUM\">20</field></shadow></value><next><block id=\"-Ud[XbNAByAN1N{7d2j4\" type=\"motion_turnleft\" ><value name=\"DEGREES\"><shadow id=\"NL)2l0#%faU7SdhqRaeM\" type=\"math_number\" ><field name=\"NUM\">30</field></shadow></value></block></next></block></next></block></value></block></xml></sprite><sprite name=\"Sprite2\"><xml><variables></variables><block id=\"ZP#%XY4NaECyD5eY30^@\" type=\"motion_movesteps\" x=\"298.5\" y=\"160.60000000000002\" ><value name=\"STEPS\"><shadow id=\"+t`[A7Wu_VwKs_ZYO[G(\" type=\"math_number\" ><field name=\"NUM\">10</field></shadow></value><next><block id=\"^d]992{@]]_c_O2(nH{l\" type=\"motion_turnright\" ><value name=\"DEGREES\"><shadow id=\"5,Iknewe=f/uiWWY-sAj\" type=\"math_number\" ><field name=\"NUM\">20</field></shadow></value><next><block id=\"A+zs}wJ;?:Sw1*kEL1-b\" type=\"motion_turnleft\" ><value name=\"DEGREES\"><shadow id=\"CWxDo7c/b/SP#}R:hLt]\" type=\"math_number\" ><field name=\"NUM\">30</field></shadow></value></block></next></block></next></block></xml></sprite></program>";
		Program program = sb3Parser.parseProgram(xml);
		CloneStmtSeqAnalyzer analyzer = new CloneStmtSeqAnalyzer();
		Map<BlockSeq, List<CStmtSeq>> res = analyzer.analyze(program);
		CloneSeqResult result = CloneSeqResult.of(res);
		assertThat(result.numGroup(), equalTo(1));
		assertThat(result.firstGroup().size(), equalTo(3));
		LOGGER.debug(result.firstGroup());
		assertThat(result.firstGroup().firstInstance().asNameSeq(),
				contains("motion_movesteps", "motion_turnright", "motion_turnleft"));

		List<Stmt> stmts = result.firstGroup().firstInstance().get();
		List<String> blockIds = stmts.stream().map(stmt -> (ExprStmt) stmt).map(stmt -> stmt.getBlockId())
				.collect(Collectors.toList());
		System.out.println("A clone instance : "+blockIds);
	}
	
	@Test
	public void testCloneAnalysisWithJsonInput() {
		String jsonStr = "{\"targets\":[{\"isStage\":true,\"name\":\"Stage\",\"variables\":{\"`jEk@4|i[#Fk?(8x)AV.-my variable\":[\"my variable\",0],\"Y})TGj3O`[./JoG:jwvt\":[\"1234\",\"0\"]},\"lists\":{},\"broadcasts\":{},\"blocks\":{},\"comments\":{},\"currentCostume\":0,\"costumes\":[{\"assetId\":\"cd21514d0531fdffb22204e0ec5ed84a\",\"name\":\"backdrop1\",\"md5ext\":\"cd21514d0531fdffb22204e0ec5ed84a.svg\",\"dataFormat\":\"svg\",\"rotationCenterX\":1,\"rotationCenterY\":1}],\"sounds\":[{\"assetId\":\"83a9787d4cb6f3b7632b4ddfebf74367\",\"name\":\"pop\",\"dataFormat\":\"wav\",\"format\":\"\",\"rate\":44100,\"sampleCount\":1032,\"md5ext\":\"83a9787d4cb6f3b7632b4ddfebf74367.wav\"}],\"volume\":100,\"tempo\":60,\"videoTransparency\":50,\"videoState\":\"off\"},{\"isStage\":false,\"name\":\"Sprite1\",\"variables\":{},\"lists\":{},\"broadcasts\":{},\"blocks\":{\"E36AelTfbgJ@gm{tQCog\":{\"opcode\":\"data_setvariableto\",\"next\":null,\"parent\":\"M|YrrN)uZ~Y9(pV#L.Ls\",\"inputs\":{\"VALUE\":[1,[10,\"0\"]]},\"fields\":{\"VARIABLE\":[\"1234\",\"Y})TGj3O`[./JoG:jwvt\"]},\"topLevel\":false,\"shadow\":false},\"@/O)M4y8_($x~d]L}$U6\":{\"opcode\":\"motion_movesteps\",\"next\":\"M?{T{,6@P8:wyMb;`X*M\",\"parent\":null,\"inputs\":{\"STEPS\":[3,\"4qq%ah84}j5|}qj1ng+p\",[4,\"10\"]]},\"fields\":{},\"topLevel\":true,\"shadow\":false,\"x\":235,\"y\":116},\"4qq%ah84}j5|}qj1ng+p\":{\"opcode\":\"operator_random\",\"next\":null,\"parent\":\"@/O)M4y8_($x~d]L}$U6\",\"inputs\":{\"FROM\":[1,[4,\"1\"]],\"TO\":[1,[4,\"10\"]]},\"fields\":{},\"topLevel\":false,\"shadow\":false},\"M?{T{,6@P8:wyMb;`X*M\":{\"opcode\":\"motion_turnright\",\"next\":\"CS=LrI+0EGR0`fvUp8:P\",\"parent\":\"@/O)M4y8_($x~d]L}$U6\",\"inputs\":{\"DEGREES\":[1,[4,\"15\"]]},\"fields\":{},\"topLevel\":false,\"shadow\":false},\"CS=LrI+0EGR0`fvUp8:P\":{\"opcode\":\"motion_turnleft\",\"next\":\"SU%Y#S$q{]wh|L7fo{w=\",\"parent\":\"M?{T{,6@P8:wyMb;`X*M\",\"inputs\":{\"DEGREES\":[1,[4,\"15\"]]},\"fields\":{},\"topLevel\":false,\"shadow\":false},\"SU%Y#S$q{]wh|L7fo{w=\":{\"opcode\":\"data_setvariableto\",\"next\":null,\"parent\":\"CS=LrI+0EGR0`fvUp8:P\",\"inputs\":{\"VALUE\":[1,[10,\"0\"]]},\"fields\":{\"VARIABLE\":[\"1234\",\"Y})TGj3O`[./JoG:jwvt\"]},\"topLevel\":false,\"shadow\":false},\"e3*{*}+puG*N!QuUe1r|\":{\"opcode\":\"motion_turnright\",\"next\":\"M|YrrN)uZ~Y9(pV#L.Ls\",\"parent\":null,\"inputs\":{\"DEGREES\":[1,[4,\"15\"]]},\"fields\":{},\"topLevel\":true,\"shadow\":false,\"x\":679,\"y\":235},\"M|YrrN)uZ~Y9(pV#L.Ls\":{\"opcode\":\"motion_turnleft\",\"next\":\"E36AelTfbgJ@gm{tQCog\",\"parent\":\"e3*{*}+puG*N!QuUe1r|\",\"inputs\":{\"DEGREES\":[1,[4,\"15\"]]},\"fields\":{},\"topLevel\":false,\"shadow\":false}},\"comments\":{},\"currentCostume\":0,\"costumes\":[{\"assetId\":\"09dc888b0b7df19f70d81588ae73420e\",\"name\":\"costume1\",\"bitmapResolution\":1,\"md5ext\":\"09dc888b0b7df19f70d81588ae73420e.svg\",\"dataFormat\":\"svg\",\"rotationCenterX\":47,\"rotationCenterY\":55},{\"assetId\":\"3696356a03a8d938318876a593572843\",\"name\":\"costume2\",\"bitmapResolution\":1,\"md5ext\":\"3696356a03a8d938318876a593572843.svg\",\"dataFormat\":\"svg\",\"rotationCenterX\":47,\"rotationCenterY\":55}],\"sounds\":[{\"assetId\":\"83c36d806dc92327b9e7049a565c6bff\",\"name\":\"Meow\",\"dataFormat\":\"wav\",\"format\":\"\",\"rate\":44100,\"sampleCount\":37376,\"md5ext\":\"83c36d806dc92327b9e7049a565c6bff.wav\"}],\"volume\":100,\"visible\":true,\"x\":0,\"y\":0,\"size\":100,\"direction\":165,\"draggable\":false,\"rotationStyle\":\"don't rotate\"},{\"isStage\":false,\"name\":\"Trees\",\"variables\":{},\"lists\":{},\"broadcasts\":{},\"blocks\":{\"r/SAl@~c.KJhpmkZ`pDF\":{\"opcode\":\"data_setvariableto\",\"next\":\"G1^19a55Lvjy#yrA5j[d\",\"parent\":null,\"inputs\":{\"VALUE\":[1,[10,\"0\"]]},\"fields\":{\"VARIABLE\":[\"1234\",\"Y})TGj3O`[./JoG:jwvt\"]},\"topLevel\":true,\"shadow\":false,\"x\":243,\"y\":96},\"G1^19a55Lvjy#yrA5j[d\":{\"opcode\":\"data_changevariableby\",\"next\":\"!2cvzsu;@JhqPwzThlO|\",\"parent\":\"r/SAl@~c.KJhpmkZ`pDF\",\"inputs\":{\"VALUE\":[1,[4,\"1\"]]},\"fields\":{\"VARIABLE\":[\"1234\",\"Y})TGj3O`[./JoG:jwvt\"]},\"topLevel\":false,\"shadow\":false},\"!2cvzsu;@JhqPwzThlO|\":{\"opcode\":\"data_changevariableby\",\"next\":\"V+)c,1?u#R;{ojsq*j)M\",\"parent\":\"G1^19a55Lvjy#yrA5j[d\",\"inputs\":{\"VALUE\":[1,[4,\"1\"]]},\"fields\":{\"VARIABLE\":[\"1234\",\"Y})TGj3O`[./JoG:jwvt\"]},\"topLevel\":false,\"shadow\":false},\"V+)c,1?u#R;{ojsq*j)M\":{\"opcode\":\"data_changevariableby\",\"next\":\"728wFTnx}oJf~%1!x=rx\",\"parent\":\"!2cvzsu;@JhqPwzThlO|\",\"inputs\":{\"VALUE\":[1,[4,\"1\"]]},\"fields\":{\"VARIABLE\":[\"1234\",\"Y})TGj3O`[./JoG:jwvt\"]},\"topLevel\":false,\"shadow\":false},\"728wFTnx}oJf~%1!x=rx\":{\"opcode\":\"data_changevariableby\",\"next\":\"d_ZqRTo1|pWKa]XlvorA\",\"parent\":\"V+)c,1?u#R;{ojsq*j)M\",\"inputs\":{\"VALUE\":[1,[4,\"1\"]]},\"fields\":{\"VARIABLE\":[\"1234\",\"Y})TGj3O`[./JoG:jwvt\"]},\"topLevel\":false,\"shadow\":false},\"d_ZqRTo1|pWKa]XlvorA\":{\"opcode\":\"data_changevariableby\",\"next\":\"mUn1UDqpsalM_-/;H)w/\",\"parent\":\"728wFTnx}oJf~%1!x=rx\",\"inputs\":{\"VALUE\":[1,[4,\"1\"]]},\"fields\":{\"VARIABLE\":[\"1234\",\"Y})TGj3O`[./JoG:jwvt\"]},\"topLevel\":false,\"shadow\":false},\"mUn1UDqpsalM_-/;H)w/\":{\"opcode\":\"data_changevariableby\",\"next\":\"zSuuoa+|e4OZ.kG0AH,%\",\"parent\":\"d_ZqRTo1|pWKa]XlvorA\",\"inputs\":{\"VALUE\":[1,[4,\"1\"]]},\"fields\":{\"VARIABLE\":[\"1234\",\"Y})TGj3O`[./JoG:jwvt\"]},\"topLevel\":false,\"shadow\":false},\"zSuuoa+|e4OZ.kG0AH,%\":{\"opcode\":\"data_changevariableby\",\"next\":\"6VGdPXpqZ8B[LFlH`+2?\",\"parent\":\"mUn1UDqpsalM_-/;H)w/\",\"inputs\":{\"VALUE\":[1,[4,\"1\"]]},\"fields\":{\"VARIABLE\":[\"1234\",\"Y})TGj3O`[./JoG:jwvt\"]},\"topLevel\":false,\"shadow\":false},\"6VGdPXpqZ8B[LFlH`+2?\":{\"opcode\":\"data_changevariableby\",\"next\":\"_`xsixbi%[/x[yQpwGLp\",\"parent\":\"zSuuoa+|e4OZ.kG0AH,%\",\"inputs\":{\"VALUE\":[1,[4,\"1\"]]},\"fields\":{\"VARIABLE\":[\"1234\",\"Y})TGj3O`[./JoG:jwvt\"]},\"topLevel\":false,\"shadow\":false},\"k0|]3OwyixT#+_mgNc)g\":{\"opcode\":\"data_changevariableby\",\"next\":\"4!aQqR|*RjV,E1`bMu)6\",\"parent\":\"_`xsixbi%[/x[yQpwGLp\",\"inputs\":{\"VALUE\":[1,[4,\"1\"]]},\"fields\":{\"VARIABLE\":[\"1234\",\"Y})TGj3O`[./JoG:jwvt\"]},\"topLevel\":false,\"shadow\":false},\"_`xsixbi%[/x[yQpwGLp\":{\"opcode\":\"data_changevariableby\",\"next\":\"k0|]3OwyixT#+_mgNc)g\",\"parent\":\"6VGdPXpqZ8B[LFlH`+2?\",\"inputs\":{\"VALUE\":[1,[4,\"1\"]]},\"fields\":{\"VARIABLE\":[\"1234\",\"Y})TGj3O`[./JoG:jwvt\"]},\"topLevel\":false,\"shadow\":false},\"4!aQqR|*RjV,E1`bMu)6\":{\"opcode\":\"data_changevariableby\",\"next\":null,\"parent\":\"k0|]3OwyixT#+_mgNc)g\",\"inputs\":{\"VALUE\":[1,[4,\"1\"]]},\"fields\":{\"VARIABLE\":[\"1234\",\"Y})TGj3O`[./JoG:jwvt\"]},\"topLevel\":false,\"shadow\":false}},\"comments\":{},\"currentCostume\":0,\"costumes\":[{\"assetId\":\"a5053b5f2e662a7ca624012b2fed8253\",\"name\":\"trees-a\",\"bitmapResolution\":1,\"md5ext\":\"a5053b5f2e662a7ca624012b2fed8253.svg\",\"dataFormat\":\"svg\",\"rotationCenterX\":49,\"rotationCenterY\":94},{\"assetId\":\"10fd8cad9a5f03d4131fc51d1b091026\",\"name\":\"trees-b\",\"bitmapResolution\":1,\"md5ext\":\"10fd8cad9a5f03d4131fc51d1b091026.svg\",\"dataFormat\":\"svg\",\"rotationCenterX\":36,\"rotationCenterY\":87}],\"sounds\":[{\"assetId\":\"83a9787d4cb6f3b7632b4ddfebf74367\",\"name\":\"pop\",\"dataFormat\":\"wav\",\"format\":\"\",\"rate\":44100,\"sampleCount\":1032,\"md5ext\":\"83a9787d4cb6f3b7632b4ddfebf74367.wav\"}],\"volume\":100,\"visible\":true,\"x\":117.82845188,\"y\":41.99441341,\"size\":100,\"direction\":90,\"draggable\":false,\"rotationStyle\":\"all around\"}],\"meta\":{\"semver\":\"3.0.0\",\"vm\":\"0.1.0-prerelease.1529422910\",\"agent\":\"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36\"}}";
		Sb3Json2Block jsonParser = new Sb3Json2Block();
		Program program = jsonParser.parseAsProgram(jsonStr);
		CloneStmtSeqAnalyzer analyzer = new CloneStmtSeqAnalyzer();
		Map<BlockSeq, List<CStmtSeq>> res = analyzer.analyze(program);
		CloneSeqResult result = CloneSeqResult.of(res);
		LOGGER.debug(result.firstGroup());
		List<Stmt> stmts = result.firstGroup().firstInstance().get();
		List<String> blockIds = stmts.stream().map(stmt -> (ExprStmt) stmt).map(stmt -> stmt.getBlockId())
				.collect(Collectors.toList());
		System.out.println("A clone instance : "+blockIds);
	}

}
