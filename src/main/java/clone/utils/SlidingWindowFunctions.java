package clone.utils;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;

public class SlidingWindowFunctions {

	public static <T> List<List<T>> slidingWindowOfAll(List<T> inputItems, Function<List<T>, Boolean> checkIfShouldInclude) {
		List<List<T>> itemRangeList = new ArrayList<>();
		for (int size = 1; size <= inputItems.size(); size++) {
			itemRangeList.addAll(slidingWindowOf(inputItems, size, checkIfShouldInclude));
		}
		return itemRangeList;
	}

	public static <T> List<List<T>> slidingWindowOf(List<T> inputItems, int size,
			Function<List<T>, Boolean> checkIfShouldInclude) {
		List<List<T>> itemRangeList = new ArrayList<>();
		int start = 0;
		int end = inputItems.size();
		while (start < end) {
			int back = start + size;
			if (back > end) {
				break;
			}
			List<T> itemRange = new ArrayList<T>();
			for (int current = start; current < back; current++) {
				itemRange.add(inputItems.get(current));
			}
			if (checkIfShouldInclude.apply(itemRange)) {
				itemRangeList.add(itemRange);
			}
			start++;
		}
		return itemRangeList;
	}

}
