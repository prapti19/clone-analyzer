package clone.data;

import java.util.List;

/*
 * interface for candidate element wrapper
 */
public interface CandidateSeq<T,P> {


	public List<T> get();

	public int size();
	
	public int hash();

	public P getParent();


	public int startingIndex();
	
	public static int priority(CandidateSeq<?,?> elm1, CandidateSeq<?,?> elm2) {
		int seqPriority = elm1.size() - elm2.size();
		if (seqPriority == 0) {
			seqPriority = elm2.startingIndex() - elm1.startingIndex();
		}
		return seqPriority;
	}

}
