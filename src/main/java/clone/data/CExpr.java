package clone.data;

import java.util.List;

import ast.ASTNode;
import ast.Expr;
import clone.analyzer.SerializationConfig;
import clone.analyzer.ASTNodeHasher;
import clone.analyzer.ASTNodeSerializer;

/*
 * * ASTExpr collectors:	=> *  collect all expressions => Wrap in CCExpr
 */

public class CExpr implements CandidateExpr<Expr> {
	private final Expr expr;

	public CExpr(Expr expr) {
		this.expr = expr;
	}

	@Override
	public Expr get() {
		return expr;
	}

	@Override
	public int size() {
		SerializationConfig config = new SerializationConfig.Builder().skipField(false).skipInput(false).build();
		List<ASTNode> serialized = new ASTNodeSerializer(config).serialize(this.expr);
		return serialized.size();
	}

	@Override
	public int hash() {
		SerializationConfig config = new SerializationConfig.Builder().skipField(false).skipInput(false).build();
		List<ASTNode> serialized = new ASTNodeSerializer(config).serialize(this.expr);
		return new ASTNodeHasher().hashOf(serialized);
	}

	@Override
	public Expr getParent() {
		return expr.rootExpr();
	}

	@Override
	public void setParent(Expr parent) {
		// TODO Auto-generated method stub

	}

	@Override
	public int startingIndex() {
		// think what needs as the basis for breaking tie
		// distance from rootExpr the closer
		// TODO Auto-generated method stub
		return 0;
	}

	public static int priority(CExpr elm1, CExpr elm2) {
		int priority = elm1.size() - elm2.size();
		if (priority == 0) {
			priority = elm2.startingIndex() - elm1.startingIndex();
		}
		return priority;
	}

	@Override
	public String toString() {
		return "CCExpr [" + (expr != null ? "expr=" + expr : "") + "]";
	}

}
