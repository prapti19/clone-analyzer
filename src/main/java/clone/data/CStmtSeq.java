package clone.data;

import java.util.List;

import com.google.common.collect.Lists;

import ast.ASTNode;
import ast.BlockSeq;
import ast.Stmt;
import clone.analyzer.SerializationConfig;
import clone.analyzer.ASTNodeHasher;
import clone.analyzer.ASTNodeSerializer;

/*
 *  ASTStmt collectors:
 * 	collect a range of seq of statements from all BlockSeq (containing valid flow thru sequence of blocks) and apply sliding windows
 * Wrap in CCStmtSeq
 */

public class CStmtSeq implements CandidateSeq<Stmt, BlockSeq> {
	private final BlockSeq parent;
	private final List<Stmt> subseq;

	public CStmtSeq(List<Stmt> subseq, BlockSeq parent) {
		this.subseq = subseq;
		this.parent = parent;
	}

	public List<Stmt> get() {
		return subseq;
	}

	public int size() {
		return subseq.size();
	}

	public int hash() {
		SerializationConfig config = new SerializationConfig.Builder().skipField(false).skipInput(true).build();
		List<ASTNode> serialized = new ASTNodeSerializer(config).serializeASTSeq(subseq);		
		return new ASTNodeHasher().hashOf(serialized);
	}

	public BlockSeq getParent() {
		return parent;
	}

	public int startingIndex() {
		return parent.getIndexOfChild(subseq.get(0));
	}

	public static int priority(CStmtSeq elm1, CStmtSeq elm2) {
		int seqPriority = elm1.size() - elm2.size();
		if (seqPriority == 0) {
			seqPriority = elm2.startingIndex() - elm1.startingIndex();
		}
		return seqPriority;
	}

	@Override
	public String toString() {
		return "StmtSeq [" + (subseq != null ? "subseq=" + subseq : "") + "]";
	}

}
