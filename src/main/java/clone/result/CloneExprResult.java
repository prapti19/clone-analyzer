package clone.result;

import static java.util.stream.Collectors.groupingBy;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import ast.ASTNode;
import ast.BlockSeq;
import ast.Expr;
import ast.ExprStmt;
import clone.analyzer.SerializationConfig;
import clone.analyzer.ASTNodeSerializer;
import clone.data.CExpr;
import clone.data.CStmtSeq;

public class CloneExprResult {

	private final List<CloneGroup> cloneGroups;

	public CloneExprResult(Map<Expr, List<CExpr>> res) {
		Map<Integer, List<CExpr>> groupByHash = res.values().stream().flatMap(Collection::stream)
				.collect(groupingBy(CExpr::hash));
		cloneGroups = groupByHash.values().stream().map(CloneGroup::new).collect(Collectors.toList());
	}

	public static CloneExprResult of(Map<Expr, List<CExpr>> res) {
		return new CloneExprResult(res);
	}

	public int numGroup() {
		return cloneGroups.size();
	}

	public List<CloneGroup> groups() {
		return cloneGroups;
	}

	public CloneGroup firstGroup() {
		return this.cloneGroups.get(0);
	}

	public class CloneGroup {
		private final List<CExpr> group;

		public CloneGroup(List<CExpr> group) {
			this.group = group;
		}

		public int size() {
			return group.size();
		}
		
		public List<CExpr> get() {
			return this.group;
		}

		public CloneExpr firstInstance() {
			return new CloneExpr(group.get(0));
		}

		@Override
		public String toString() {
			return "CloneGroup [" + (group != null ? "group=" + group : "") + "]";
		}
	}

	public class CloneExpr {

		private final CExpr expr;

		public CloneExpr(CExpr expr) {
			this.expr = expr;
		}
		
		public Expr get() {
			return expr.get();
		}

		public int size() {
			return expr.size();
		}

		public List<String> asNameSeq() {
			SerializationConfig config = new SerializationConfig.Builder().skipField(false).skipInput(false).build();
			List<ASTNode> serialized = new ASTNodeSerializer(config).serialize(expr.get());
			List<String> names = serialized.stream().map(expr -> (Expr) expr).map(expr -> expr.toDevString())
					.collect(Collectors.toList());
			return names;
		}
	}

}
