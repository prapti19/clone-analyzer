package clone.result;

import static java.util.stream.Collectors.groupingBy;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import ast.BlockSeq;
import ast.ExprStmt;
import ast.Stmt;
import clone.data.CStmtSeq;

public class CloneSeqResult {

	private final Map<BlockSeq, List<CStmtSeq>> result;
	private final List<CloneGroup> cloneGroups;

	public CloneSeqResult(Map<BlockSeq, List<CStmtSeq>> res) {
		this.result = res;
		Map<Integer, List<CStmtSeq>> groupByHash = res.values().stream().flatMap(Collection::stream)
				.collect(groupingBy(CStmtSeq::hash));
		cloneGroups = groupByHash.values().stream().map(CloneGroup::new).collect(Collectors.toList());
	}

	public static CloneSeqResult of(Map<BlockSeq, List<CStmtSeq>> res) {
		return new CloneSeqResult(res);
	}

	public int numGroup() {
		return cloneGroups.size();
	}

	public List<CloneGroup> groups() {
		return cloneGroups;
	}

	public CloneGroup firstGroup() {
		return this.cloneGroups.get(0);
	}

	public class CloneGroup {
		private final List<CStmtSeq> group;

		public CloneGroup(List<CStmtSeq> group) {
			this.group = group;
		}

		public int size() {
			return group.size();
		}

		public CloneSeq firstInstance() {
			return new CloneSeq(group.get(0));
		}
		
		public List<CStmtSeq> getGroup() {
			return group;
		}

		@Override
		public String toString() {
			return "CloneGroup [" + (group != null ? "group=" + group : "") + "]";
		}
	}

	public class CloneSeq {

		private final CStmtSeq seq;

		public CloneSeq(CStmtSeq ccStmtSeq) {
			this.seq = ccStmtSeq;
		}

		public int size() {
			return seq.size();
		}
		
		public List<Stmt> get() {
			return seq.get();
		}

		public List<String> asNameSeq() {
			List<String> names = seq.get().stream().map(stmt -> ((ExprStmt) stmt).getOpcode())
					.collect(Collectors.toList());
			return names;
		}
	}

}
