package clone.analyzer;

import java.util.Iterator;
import java.util.List;

import com.google.common.collect.Lists;

import ast.ASTNode;
import ast.Literal;
import ast.ScratchBlock;

/**
 * ASTNodeHasher should be designed to have no knowledge of the input (serialized nodes) or a leaf node
 * It will only consider the top-level of each node of the input list
 * ASTNodeHasher accepts list of ASTNode and produce hash based on top level ast node
 * 
 * @author karn
 *
 */

public class ASTNodeHasher {
	public int hashOf(ASTNode leafNode) {
		return hashOf(Lists.newArrayList(leafNode));
	}

	public int hashOf(List<ASTNode> serializedAstNodes) {
		int hashCode = 1;
		Iterator<ASTNode> i = serializedAstNodes.iterator();
		while (i.hasNext()) {
			Object obj = i.next();
			int typeHash = 0;
			if (obj instanceof ScratchBlock) {
				typeHash = ((ScratchBlock) obj).getOpcode().hashCode();
			} else if (obj instanceof Literal) {
				String typeValueComb = obj.getClass().getSimpleName()
						+ (((Literal) obj).getValue() == null ? "" : ((Literal) obj).getValue());
				typeHash = typeValueComb.hashCode();
			}
			hashCode = 31 * hashCode + (obj == null ? 0 : typeHash);
		}
		return hashCode;
	}

}
