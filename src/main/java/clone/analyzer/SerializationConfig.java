package clone.analyzer;

import java.awt.List;
import java.util.ArrayList;

import com.google.common.collect.Lists;

import ast.ASTNode;
import ast.AttrAccess;
import ast.Expr;
import ast.NumLiteral;

/**
 * AST node serialization logic for Scratch3 ASTNode
 * 
 * @author karn
 *
 */
public class SerializationConfig {
	private final boolean skipField;
	private final boolean skipInput;

	public boolean shouldSkip(ASTNode node) {
		ArrayList<Class<? extends ASTNode>> skippables = Lists.newArrayList(ast.AttrAccess.class);
		for (Class<? extends ASTNode> klass : skippables) {

			if (node instanceof Expr) {
				return skipInput && ((Expr) node).isRootExpr();
			}

			if (klass.isInstance(node)) {
				return true;
			}
		}
		return false;
	}

	public boolean skipField() {
		return skipField;
	}

	public boolean skipInput() {
		return skipInput;
	}

	public boolean isAtomic(ASTNode node) {
		if (node instanceof AttrAccess) {
			return true;
		}
		return false;
	}

	public static class Builder {
		private boolean skipField = true;
		private boolean skipInput = false;

		public Builder skipField(boolean val) {
			skipField = val;
			return this;
		}

		public Builder skipInput(boolean val) {
			skipInput = val;
			return this;
		}

		public SerializationConfig build() {
			return new SerializationConfig(this);
		}
	}

	public static SerializationConfig defaultConfig() {
		return new SerializationConfig.Builder().skipField(false).build();
	}

	private SerializationConfig(Builder builder) {
		skipField = builder.skipField;
		skipInput = builder.skipInput;
	}
}
