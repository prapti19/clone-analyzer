package clone.analyzer;

import static clone.analyzer.CustomCollectors.toSortedList;
import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.toMap;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.core.config.Configurator;

import ast.BlockSeq;
import ast.Program;
import ast.Stmt;
import clone.data.CStmtSeq;

public class CloneStmtSeqAnalyzer {
	private static final Logger logger = LogManager.getLogger("CloneStmtSeqAnalyzer");
	int minCCGroupSize = 2;

	private Function<CStmtSeq, Integer> groupingStrategy;
	private Function<CStmtSeq, BlockSeq> parentLink;
	private Comparator<CStmtSeq> seqPriorityFunc;
	private Comparator<Entry<BlockSeq, List<CStmtSeq>>> parentPriority;
	
	public CloneStmtSeqAnalyzer() {
		Configurator.setLevel("CloneStmtSeqAnalyzer", Level.OFF);
	}
	
	public Map<BlockSeq, List<CStmtSeq>> analyze(Program program) {
		ASTCollectorFunctions collector = new ASTCollectorFunctions();
		HashSet<BlockSeq> seqColl = program.blockSeqColl();
		List<CStmtSeq> stmtSeqList = new ArrayList<>();
		for(BlockSeq seq : seqColl) {
			stmtSeqList.addAll(collector.collect(seq));
		}
		return process(stmtSeqList);
	}

	public Map<BlockSeq, List<CStmtSeq>> process(java.util.List<CStmtSeq> inputList) {
		groupingStrategy = CStmtSeq::hash;
		parentLink = CStmtSeq::getParent;
		seqPriorityFunc = CStmtSeq::priority;
		parentPriority = (entry1, entry2) -> entry1.getKey().getNumStmt() - entry2.getKey().getNumStmt();

		
		Map<Integer, List<CStmtSeq>> filteredGroups = groupAllPossibleCNodes(inputList);
		logger.info("filteredGroups:" + filteredGroups);
		Map<BlockSeq, List<CStmtSeq>> orderedMap = kvOrderingForOverlapElim(filteredGroups);
		logger.info("ordering for overlap elim:" + orderedMap);
		Map<BlockSeq, List<CStmtSeq>> resultMap = eliminateOverlapCNodes(orderedMap);

		return resultMap;
	}

	private Map<BlockSeq, List<CStmtSeq>> eliminateOverlapCNodes(Map<BlockSeq, List<CStmtSeq>> orderedByValue) {
		List<CStmtSeq> eliminated = new ArrayList<>();
		for (BlockSeq parentKey : orderedByValue.keySet()) {
			List<CStmtSeq> candidates = orderedByValue.get(parentKey);
			for (int i = 0; i < candidates.size(); i++) {
				CStmtSeq seqI = candidates.get(i);
				if (eliminated.contains(seqI)) {
					continue;
				}
				
				for (int j = i + 1; j < candidates.size(); j++) {
					CStmtSeq seqJ = candidates.get(j);
					if (eliminated.contains(seqJ)) {
						continue;
					}
					Set<Stmt> seqIElms = new HashSet<>(seqI.get());
					Set<Stmt> seqJElms = new HashSet<>(seqJ.get());
					seqIElms.retainAll(seqJElms);
					if (!seqIElms.isEmpty()) { // if not empty then it's overlapping
						logger.info(seqI + "is overlapped with"+ seqJ);
						markEliminatedCommonSeqFromMap(orderedByValue, seqJ, eliminated);
					}
				}
			}
		}

		Map<BlockSeq, List<CStmtSeq>> result = orderedByValue.entrySet().stream().collect(toMap(e -> e.getKey(),
				e -> e.getValue().stream().filter(seq -> !eliminated.contains(seq)).collect(Collectors.toList()),
				(oldValue, newValue) -> oldValue, LinkedHashMap::new));
		return result;
	}

	private void markEliminatedCommonSeqFromMap(Map<BlockSeq, List<CStmtSeq>> orderedByValue, CStmtSeq seqJ,
			List<CStmtSeq> eliminated) {
		logger.info("remove:" + seqJ);
		eliminated.add(seqJ);
		for (List<CStmtSeq> group : orderedByValue.values()) {
			logger.info("before" + group);
			group.forEach(seq -> {
				if (seq.equals(seqJ)) {
					eliminated.add(seq);
				}
			});
		}

	}

	private Map<BlockSeq, List<CStmtSeq>> kvOrderingForOverlapElim(Map<Integer, List<CStmtSeq>> filteredGroups) {
		Map<BlockSeq, List<CStmtSeq>> parentToSortedSeq = filteredGroups.values().stream().flatMap(Collection::stream)
				.collect(groupingBy(parentLink, toSortedList(seqPriorityFunc)));
		logger.info("parentToSortedSeq:" + parentToSortedSeq);

		Map<BlockSeq, List<CStmtSeq>> orderedByValue = parentToSortedSeq.entrySet().stream()
				.sorted(parentPriority.reversed())
				.collect(toMap(Entry::getKey, Entry::getValue, (oldValue, newValue) -> oldValue, LinkedHashMap::new));
		logger.info("Ordered:" + orderedByValue);
		return orderedByValue;
	}

	private Map<Integer, List<CStmtSeq>> groupAllPossibleCNodes(List<CStmtSeq> inputList) {
		Map<Integer, List<CStmtSeq>> groupByHash = inputList.stream().collect(groupingBy(groupingStrategy));
		logger.debug(groupByHash);

		Map<Integer, List<CStmtSeq>> filteredGroups = groupByHash.entrySet().stream()
				.filter(e -> e.getValue().size() >= minCCGroupSize).collect(toMap(Entry::getKey, Entry::getValue));
		logger.debug(filteredGroups);
		return filteredGroups;
	}

}
