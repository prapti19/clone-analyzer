package clone.analyzer;

import java.util.ArrayList;
import java.util.List;

import ast.ASTNode;
import ast.Stmt;

/*
 *  * ASTNodeSerializer accepts CCStmtSeq/CCExpr => list of ASTNode
 */

public class ASTNodeSerializer {
	private final SerializationConfig config;

	public ASTNodeSerializer() {
		config = SerializationConfig.defaultConfig();
	}

	public ASTNodeSerializer(SerializationConfig config) {
		this.config = config;
	}

	public List<ASTNode> serialize(ASTNode val) {
		List<ASTNode> nodes = new ArrayList<ASTNode>();
		nodes.add(val);
		serialize(val, nodes);
		return nodes;
	}

	private void serialize(ASTNode val, List<ASTNode> nodes) {
		for (int i = 0; i < val.getNumChild(); i++) {
			ASTNode child = val.getChild(i);
			if (!shouldSkip(child)) {
				if(!(child instanceof ast.List)) {
					nodes.add(child);					
				}
				serialize(child, nodes);
			}
		}
	}

	private boolean shouldSkip(ASTNode child) {
		return config.shouldSkip(child);
	}

	public List<ASTNode> serializeASTSeq(List<Stmt> subseq) {
		List<ASTNode> serialized = new ArrayList<>();
		for (ASTNode node : subseq) {
			serialized.addAll(serialize(node));
		}
		return serialized;
	}
}
