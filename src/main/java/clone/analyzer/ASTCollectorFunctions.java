package clone.analyzer;

import static clone.utils.SlidingWindowFunctions.slidingWindowOfAll;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import com.google.common.collect.Lists;

import ast.BlockSeq;
import ast.Stmt;
import clone.data.CStmtSeq;

public class ASTCollectorFunctions {

	private List<List<Stmt>> collect(Iterable<Stmt> stmtIterable) {
		ArrayList<Stmt> list = Lists.newArrayList(stmtIterable);
		List<List<Stmt>> subseqColl = slidingWindowOfAll(list, subseq -> subseq.size() > 1);
		return subseqColl;
	}

	public List<CStmtSeq> collect(BlockSeq seq) {
		List<List<Stmt>> list = collect(seq.getStmtList());
		return list.stream().map(stmtList -> new CStmtSeq(stmtList, seq)).collect(Collectors.toList());
	}

}
