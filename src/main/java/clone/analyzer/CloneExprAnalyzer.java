package clone.analyzer;

import static clone.analyzer.CustomCollectors.toSortedList;
import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.toMap;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.core.config.Configurator;

import ast.ASTNode;
import ast.Expr;
import ast.OperatorExpr;
import ast.Program;
import ast.Sprite;
import clone.data.CExpr;

public class CloneExprAnalyzer {
	private static final Logger logger = LogManager.getLogger("CloneExprAnalyzer");
	SerializationConfig config = new SerializationConfig.Builder().skipField(false).skipInput(false).build();
	ASTNodeSerializer exprSerializer = new ASTNodeSerializer(config);
	int minCCGroupSize = 2;

	private Function<CExpr, Integer> groupingStrategy;
	private Function<CExpr, Expr> parentLink;
	private Comparator<CExpr> seqPriorityFunc;
	private Comparator<Entry<Expr, List<CExpr>>> parentPriority;

	public CloneExprAnalyzer() {
		Configurator.setLevel("CloneExprAnalyzer", Level.OFF);
	}

	public Map<Expr, List<CExpr>> analyze(Program program) {
		List<CExpr> inputList = new ArrayList<>();
		for (Sprite sprite : program.getSpriteList()) {
			HashSet<OperatorExpr> opExprSet = sprite.opExprColl();
			inputList.addAll(opExprSet.stream().map(CExpr::new).collect(Collectors.toList()));

			// for(CCNodeContainable scriptKey: map.keySet()) {
			// map.get(scriptKey).stream().forEach(el ->
			// System.out.println(el.toDevString()));
			// }
		}
		return process(inputList);
	}

	public Map<Expr, List<CExpr>> process(List<CExpr> inputList) {
		groupingStrategy = CExpr::hash;
		parentLink = CExpr::getParent;
		seqPriorityFunc = CExpr::priority;
		parentPriority = (entry1, entry2) -> new CExpr(entry1.getKey()).size() - new CExpr(entry2.getKey()).size();

		Map<Integer, List<CExpr>> filteredGroups = groupAllPossibleCNodes(inputList);
		logger.info("filteredGroups:" + filteredGroups);
		Map<Expr, List<CExpr>> orderedMap = kvOrderingForOverlapElim(filteredGroups);
		logger.info("ordering for overlap elim:" + orderedMap);
		Map<Expr, List<CExpr>> resultMap = eliminateOverlapCNodes(orderedMap);

		return resultMap;
	}

	private Map<Expr, List<CExpr>> eliminateOverlapCNodes(Map<Expr, List<CExpr>> orderedByValue) {
		List<CExpr> eliminated = new ArrayList<>();
		for (Expr parentKey : orderedByValue.keySet()) {
			List<CExpr> candidates = orderedByValue.get(parentKey);
			for (int i = 0; i < candidates.size(); i++) {
				CExpr seqI = candidates.get(i);
				if (eliminated.contains(seqI)) {
					continue;
				}

				for (int j = i + 1; j < candidates.size(); j++) {
					CExpr seqJ = candidates.get(j);
					if (eliminated.contains(seqJ)) {
						continue;
					}

					Set<ASTNode> seqIElms = new HashSet<>(exprSerializer.serialize(seqI.get()));
					Set<ASTNode> seqJElms = new HashSet<>(exprSerializer.serialize(seqJ.get()));
					seqIElms.retainAll(seqJElms);
					if (!seqIElms.isEmpty()) { // if not empty then it's overlapping
						logger.info(seqI + "is overlapped with" + seqJ);
						markEliminatedCommonSeqFromMap(orderedByValue, seqJ, eliminated);
					}
				}
			}
		}

		Map<Expr, List<CExpr>> result = orderedByValue.entrySet().stream().collect(toMap(e -> e.getKey(),
				e -> e.getValue().stream().filter(seq -> !eliminated.contains(seq)).collect(Collectors.toList()),
				(oldValue, newValue) -> oldValue, LinkedHashMap::new));
		return result;
	}

	private void markEliminatedCommonSeqFromMap(Map<Expr, List<CExpr>> orderedByValue, CExpr seqJ,
			List<CExpr> eliminated) {
		logger.info("remove:" + seqJ);
		eliminated.add(seqJ);
		for (List<CExpr> group : orderedByValue.values()) {
			logger.info("before" + group);
			group.forEach(seq -> {
				if (seq.equals(seqJ)) {
					eliminated.add(seq);
				}
			});
		}

	}

	private Map<Integer, List<CExpr>> groupAllPossibleCNodes(List<CExpr> inputList) {
		Map<Integer, List<CExpr>> groupByHash = inputList.stream().collect(groupingBy(groupingStrategy));
		logger.debug(groupByHash);

		Map<Integer, List<CExpr>> filteredGroups = groupByHash.entrySet().stream()
				.filter(e -> e.getValue().size() >= minCCGroupSize).collect(toMap(Entry::getKey, Entry::getValue));
		logger.debug(filteredGroups);
		return filteredGroups;
	}

	private Map<Expr, List<CExpr>> kvOrderingForOverlapElim(Map<Integer, List<CExpr>> filteredGroups) {
		Map<Expr, List<CExpr>> parentToSortedSeq = filteredGroups.values().stream().flatMap(Collection::stream)
				.collect(groupingBy(parentLink, toSortedList(seqPriorityFunc)));
		logger.info("parentToSortedSeq:" + parentToSortedSeq);

		Map<Expr, List<CExpr>> orderedByValue = parentToSortedSeq.entrySet().stream().sorted(parentPriority.reversed())
				.collect(toMap(Entry::getKey, Entry::getValue, (oldValue, newValue) -> oldValue, LinkedHashMap::new));
		logger.info("Ordered:" + orderedByValue);
		return orderedByValue;
	}

}
